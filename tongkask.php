<?php

define('TONGKASK_ROOT', getcwd());

const TONGKASK_CONFIG_ROOT   = TONGKASK_ROOT . DIRECTORY_SEPARATOR . 'config.yaml';
const TONGKASK_COMPOSER_ROOT = TONGKASK_ROOT . DIRECTORY_SEPARATOR . 'vendor/coralframe/studio/src';

require_once './vendor/autoload.php';

use App\Route\HttpRoute;
use App\Route\WebsocketRoute;
use Swoole\WebSocket\Server;
use TongkaskFrame\Frame\HttpBasic\HttpRequest;
use TongkaskFrame\Frame\HttpServer;
use TongkaskFrame\Frame\TaskProcess;
use TongkaskFrame\Main;
use TongkaskFrame\Struct\HttpCode;
use TongkaskFrame\TongkaskException;

try {
    $Main = new Main($argc, $argv);
    $Main->SetHttpCallBack(function (HttpServer $server) {
        $server->SetHttpServerErrorCallback(function (HttpRequest $HttpRequest, Throwable $th) {
            $Code          = new HttpCode();
            $Code->Code    = TongkaskException::System_ERROR_CODE;
            $Code->Message = $th->getMessage();
            $HttpRequest->GetResponse()->Write(json_encode($Code));
            $HttpRequest->GetResponse()->End();
        });
        $server->SetWebsocketOpenErrorCallback(function (Server $server, HttpRequest $HttpRequest, Throwable $th) {
            $Code          = new HttpCode();
            $Code->Code    = TongkaskException::System_ERROR_CODE;
            $Code->Message = $th->getMessage();
            $fd            = $HttpRequest->GetFd();
            $server->push($fd, json_encode($Code));
        });
        $server->SetHttpRouteList(HttpRoute::SetHttpRouteList());
        $server->SetWebsocketRouteList(WebsocketRoute::SetWebsocketRouteList());
        $server->Start();
    });
    $Main->SetTaskCallBack(function (TaskProcess $TaskProcess) {
        $TaskProcess->SetTaskList([
            ['ProcessTest', Process\Test::class, 'test'],
            ['CrontabTest', Crontab\Test::class, 'test', 1, 4],
        ]);
    });
    $Main->Run();

} catch (Throwable $th) {
    var_dump($th->getCode());
    var_dump($th->getMessage());
    var_dump($th->getTrace());
}