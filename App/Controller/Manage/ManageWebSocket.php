<?php

namespace App\Controller\Manage;

use TongkaskFrame\Frame\HttpBasic\HttpWebsocket;
use TongkaskFrame\Tool\Instance\WebSocketConnectionInstance;

class ManageWebSocket extends HttpWebsocket
{
    public function index()
    {
        $fd = $this->GetFd();
        WebSocketConnectionInstance::getInstance()::SetClass($fd, '\App\Controller\Manage\ManageWebSocket');
        WebSocketConnectionInstance::getInstance()::SetAction($fd, 'onMessage');
    }

    public function onMessage()
    {
        $fd            = $this->GetFd();
        $websocektConn = WebSocketConnectionInstance::getInstance()::GetConn($fd);
        var_dump($websocektConn->Frame->data);
        $websocektConn->Server->push($fd, '你好');
    }
}