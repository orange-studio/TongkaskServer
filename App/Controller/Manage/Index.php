<?php

namespace App\Controller\Manage;

use JsonException;
use TongkaskFrame\Frame\Controller\BaseController;

class Index extends BaseController
{
    /**
     * @throws JsonException
     */
    public function index()
    {
        $this->ReturnJson(0, '123');
    }
}