<?php

namespace App\Route;

use App\Controller\Manage\Index;
use App\Middleware\ManageMiddleware;
use Exception;
use TongkaskFrame\Component\Route\HttpRouteCollector;
use TongkaskFrame\Tool\Instance\RouterHttpInstance;

class HttpRoute
{
    /**
     * @throws Exception
     */
    public static function SetHttpRouteList(): HttpRouteCollector
    {
        $route = RouterHttpInstance::getInstance();
        $route->AddGroup('/manage', function (HttpRouteCollector $r) {
            $r->POST('index', Index::class, 'index')->AddMiddleware(ManageMiddleware::class, 'ManageMiddlewareSign');
        });
        return $route;
    }
}