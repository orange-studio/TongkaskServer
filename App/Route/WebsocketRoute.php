<?php

namespace App\Route;

use Exception;
use TongkaskFrame\Component\Route\WebsocketRouteCollector;
use TongkaskFrame\Tool\Instance\RouterWebsocketInstance;

class WebsocketRoute
{
    /**
     * @throws Exception
     */
    public static function SetWebsocketRouteList(): WebsocketRouteCollector
    {
        $route = RouterWebsocketInstance::getInstance();
        $route->GET('/websocekt', '\App\Controller\Manage\ManageWebSocket', 'index');
        return $route;
    }
}